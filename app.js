var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');

var routes = require('./routes/index');
var users = require('./routes/users');
var uploads = require('./routes/uploads');
var downloads = require('./routes/downloads');
var tracker_scoring = require('./routes/tracker_scoring');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(multer({dest:'./public/assets'}).fields([
    { name: 'myFile', maxCount: 5  }
    ]));

//routes for files and video uploads
//app.use('/', routes);
app.use('/users', users);
app.use("/uploads", uploads);
app.use("/download/:id", downloads.getByID);

//routes for tracker_scoring
app.get('/all_scores',tracker_scoring.findAll); //list all scores
app.get('/get_score/:id', tracker_scoring.findById); //get score by ID
app.post('/new_score/', tracker_scoring.addScore);
app.delete('/delete_score/:id', tracker_scoring.deleteScore); //delete the score


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



module.exports = app;
