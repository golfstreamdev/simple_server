/**
 * Last edited by Carlyn on 9/19/15.
 */
var express = require('express');
var router = express.Router();
var util = require("util");
var fs = require("fs");

router.get('/', function(req, res) {
    res.render("uploadPage", {title: "Upload your file here"});
});

router.post("/upload", function(req, res, next){
    if (req.files) {
        console.log(util.inspect(req.files));
        if (req.files.myFile.size === 0) {
            return next(new Error("Hey, first would you select a file?"));
        }
        var element_str="";

        req.files.myFile.forEach(
            function(element)
            {


                fs.exists(element.path, function(exists) {
                    if(exists) {
                        console.log(element)
                    } else {
                        res.end("Well, there is no magic for those who don’t believe in it!");
                    }
                });
            });
        res.send(req.files.myFile);



    }
});

module.exports = router;