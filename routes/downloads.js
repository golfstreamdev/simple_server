/**
 * Last edited by Carlyn on 9/19/15.
 */
fs = require('fs');



exports.getByID  = function(req, res) {
    var id = req.params.id;
    fs.readFile('./public/assets/' + id, function (err, data) {
        if (err) {
             console.log(err);
            res.send(err);
        }
        res.send(data);
    });
}